//
//  PostListViewModel.swift
//  AWPosts
//
//  Created by Mohammed Awais on 02/05/24.
//

import Foundation
import Combine

class PostListViewModel {

    let subject = CurrentValueSubject<Bool,Never>(false)
    var posts : [Post] = [Post]()
    
    var numberOfPosts: Int {
        return posts.count
    }

    func loadPosts() async throws {
        
        guard self.posts.count != 100 else { return }

        let posts = try! await Webservice.shared.fetchPosts()
        self.posts.append(contentsOf: posts)
        self.subject.send(true)
    }
}
