//
//  Post.swift
//  AWPosts
//
//  Created by Mohammed Awais on 02/05/24.
//

import Foundation

struct Post : Codable {
    
    let userId : Int
    let id : Int
    let title : String
    let body : String
}
