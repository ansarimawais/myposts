//
//  PostDetailViewController.swift
//  MyPosts
//
//  Created by Mohammed Awais on 02/05/24.
//

import Foundation
import UIKit

class PostDetailViewController: UIViewController {
    
    @IBOutlet weak var id : UILabel!
    @IBOutlet weak var postTitle : UILabel!
    @IBOutlet weak var userID : UILabel!
    @IBOutlet weak var body : UILabel!

    
    var post: MyCellViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let postData = post {
            self.id.text = "\(postData.id)"
            self.postTitle.text = postData.title
            self.userID.text = "\(postData.userID)"
            self.body.text = postData.body
        }
    }
}
