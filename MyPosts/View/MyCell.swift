//
//  Views.swift
//  AWPosts
//
//  Created by Mohammed Awais on 02/05/24.
//

import Foundation
import UIKit

class MyCell : UITableViewCell {
    
    @IBOutlet weak var id : UILabel!
    @IBOutlet weak var title : UILabel!
    @IBOutlet weak var userID : UILabel!
    @IBOutlet weak var body : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func configure(viewModel : MyCellViewModel)
    {
        self.title.text = viewModel.title
        self.id.text = "\(viewModel.id)"
        self.userID.text = "\(viewModel.userID)"
        self.body.text = "\(viewModel.body)"
    }
    
}

struct MyCellViewModel{
    let title : String
    let id : Int
    let body : String
    let userID : Int
}
