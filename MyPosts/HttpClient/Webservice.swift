//
//  Webservice.swift
//  AWPosts
//
//  Created by Mohammed Awais on 02/05/24.
//

import Foundation

class Webservice {
    
    static let shared = Webservice()
    private init(){}
    private var posts: [Post] = []
    private var isFetching = false
    private let urlSession = URLSession(configuration: .default)
    private var currentPage = 0
    private var hasMorePosts = true


//    func fetchPosts() async throws -> [Post]{
//        print("self.currentPage ", self.currentPage)
//        guard !isFetching && hasMorePosts else {
//            
//            return []
//        }
//
//        isFetching = true
//        let urlString = "https://jsonplaceholder.typicode.com/posts?_page=\(currentPage + 1)&_limit=10"
//        guard let url = URL(string: urlString) else {
//            return []
//        }
//
//        
//        do
//        {
//            let (data, _) = try await URLSession.shared.data(for: URLRequest(url: url))
//            self.isFetching = false
//            let posts = try JSONDecoder().decode([Post].self, from: data)
//            if posts.isEmpty {
//                hasMorePosts = false
//                return []
//            }
//            
//            self.currentPage += 1
//            self.posts.append(contentsOf: posts)
//            
//            
//            return posts
//        }
//        catch (let error){
//            print(error.localizedDescription)
//            return []
//        }
//    }
    
    func fetchPosts() async throws -> [Post] {
        print("Current Page: ", self.currentPage)
        guard !isFetching && hasMorePosts else {
            return []
        }

        isFetching = true
        let pageKey = NSString(string: "posts_page_\(currentPage + 1)")
        do {
            let posts: [Post] = try await Memoizer.shared.memoize(key: pageKey) {
                let urlString = "https://jsonplaceholder.typicode.com/posts?_page=\(currentPage + 1)&_limit=10"
                guard let url = URL(string: urlString) else {
                    throw NSError(domain: "InvalidURL", code: 100, userInfo: nil)
                }

                let (data, _) = try await URLSession.shared.data(for: URLRequest(url: url))
                return try JSONDecoder().decode([Post].self, from: data)
            }
            
            guard !posts.isEmpty else {
                isFetching = false
                hasMorePosts = false
                return []
            }
            
            currentPage += 1
            self.posts.append(contentsOf: posts)
            isFetching = false
            
            return posts
        } catch {
            isFetching = false
            print(error.localizedDescription)
            throw error
        }
    }

}
