//
//  ViewController.swift
//  MyPosts
//
//  Created by Mohammed Awais on 02/05/24.
//

import UIKit
import Combine

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var myCellViewModels : [MyCellViewModel] = [MyCellViewModel]()
    var cancellable = Set<AnyCancellable>()

    let viewModel = PostListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.title = "Posts"
        Task{
            try! await viewModel.loadPosts()
        }
        viewModel.subject.sink { status in
            if status {
                self.myCellViewModels = self.viewModel.posts.map { MyCellViewModel(title: $0.title, id: $0.id, body: $0.body, userID: $0.userId) }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }.store(in: &cancellable)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let detailVC = segue.destination as? PostDetailViewController,
               let indexPath = tableView.indexPathForSelectedRow {
                // Get the data for the selected row
                let selectedPost = myCellViewModels[indexPath.row]
                detailVC.post = selectedPost
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }


}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.posts.count
    }
    
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MyCell else { 
                return UITableViewCell()
            }
            cell.configure(viewModel: myCellViewModels[indexPath.row])
            return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.numberOfPosts - 1{
            Task{
                try! await viewModel.loadPosts()
            }
        }
    }
}



