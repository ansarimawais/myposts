//
//  Memoizer.swift
//  MyPosts
//
//  Created by Mohammed Awais on 03/05/24.
//

import Foundation

class Memoizer {
    static let shared = Memoizer()
    private var cache = NSCache<NSString, AnyObject>()

    private init() {}

    func memoize<T: Codable>(key: NSString, operation: () async throws -> T) async throws -> T {
        if let cachedValue = cache.object(forKey: key) as? T {
            return cachedValue
        } else {
            let result = try await operation()
            cache.setObject(result as AnyObject, forKey: key)
            return result
        }
    }
}
